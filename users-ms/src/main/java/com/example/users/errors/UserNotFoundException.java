package com.example.users.errors;

import com.example.common.exception.NotFoundException;

public class UserNotFoundException extends NotFoundException {

    public static final String MESSAGE = "User with id %s not Found.";
    private static final long serialVersionUID = 5843213248811L;

    public UserNotFoundException(Long userId) {
        super(String.format(MESSAGE, userId));
    }
}
