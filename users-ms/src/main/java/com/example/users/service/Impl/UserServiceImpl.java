package com.example.users.service.Impl;

import com.example.common.domain.User;
import com.example.users.dto.RegisterUserDto;
import com.example.users.dto.UpdateUserDto;
import com.example.users.errors.UserNotFoundException;
import com.example.users.mapper.UserMapper;
import com.example.users.repository.UserRepository;
import com.example.users.service.KafkaMsgPublisher;
import com.example.users.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl  implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final KafkaMsgPublisher kafkaMsgPublisher;

    @Value("${spring.kafka.topics.registration}")
    private String registrationTopic;

    @Override
    public User register(RegisterUserDto userVm) {
        User user=userRepository.save(userMapper.dtoToEntity(userVm));
        kafkaMsgPublisher.publish(null,user, "user-ms");
        return user;
    }

    @Override
    public User update(Long id, UpdateUserDto updateUserDto) {
        userRepository.findById(id).orElseThrow(()-> new UserNotFoundException(id));
        User user=userMapper.dtoToEntityUpddate(updateUserDto);
        user.setId(id);
        return userRepository.save(user);
    }
}
