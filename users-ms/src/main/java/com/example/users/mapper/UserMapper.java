package com.example.users.mapper;


import com.example.common.domain.User;
import com.example.users.dto.RegisterUserDto;
import com.example.users.dto.UpdateUserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface UserMapper {

    @Mapping(target = "id", ignore = true)
    User dtoToEntity(RegisterUserDto dto);

    @Mapping(target = "id", ignore = true)
    User dtoToEntityUpddate(UpdateUserDto dto);
}
