package com.example.cards.service.Impl;

import com.example.cards.repository.UserRepository;
import com.example.common.domain.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaUserMsgRead {

    private final UserRepository userRepository;


    @KafkaListener(topics = "user-ms", groupId = "cards-ms",containerFactory = "kafkaJsonListenerContainerFactory")
    public void listenGroupFoo(User user) throws InterruptedException {
        userRepository.save(user);
        log.info("Received Message in group ingress1 {} ", user);
    }
    //
}
