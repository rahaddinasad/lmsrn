package com.example.users.service;

import com.example.common.domain.User;
import com.example.users.dto.RegisterUserDto;
import com.example.users.dto.UpdateUserDto;

public interface UserService {

    User register(RegisterUserDto userVm);

    User update(Long id, UpdateUserDto updateUserDto);

}
