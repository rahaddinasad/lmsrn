package com.example.users.controller;

import com.example.common.domain.User;
import com.example.users.dto.RegisterUserDto;
import com.example.users.dto.UpdateUserDto;
import com.example.users.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/save")
    public ResponseEntity<User> save(@RequestBody  RegisterUserDto registerUserDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.register(registerUserDto));
    }

    @PutMapping("/{id}")
    public  ResponseEntity<User> update(@PathVariable Long id, @RequestBody UpdateUserDto updateUserDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.update(id,updateUserDto));
    }

}
