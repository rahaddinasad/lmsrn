package com.example.users.config;


import com.example.common.config.KafkaProducerConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({KafkaProducerConfig.class})
public class CommonConfig {

}
