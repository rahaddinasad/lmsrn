package com.example.loans.config;


import com.example.common.config.KafkaConsumerConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Import({KafkaConsumerConfig.class})
@Configuration
public class CommonConfig {

}
