package com.example.loans.service.Impl;

import com.example.common.domain.User;
import com.example.loans.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaUserMsgRead {

    private final UserRepository userRepository;


    @KafkaListener(topics = "user-ms", groupId = "loans-ms",containerFactory = "kafkaJsonListenerContainerFactory")
    public void listenGroupFoo(User user) throws InterruptedException {
        userRepository.save(user);
        log.info("Received Message in group ingress1 {} ", user);
       /* Thread.sleep((long) (Math.random() * 10000));*/
    }
    //
}
