package com.example.users.dto;

import lombok.Data;

@Data
public class UpdateUserDto {
    private String displayName;
    private String email;
    private String password;
}
